//
//  ContentView.swift
//  enums-for-tags
//
//  Created by Maegan Wilson on 5/14/20.
//  Copyright © 2020 Maegan Wilson. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = Tabs.inbox
    
    private enum Tabs: Hashable {
        case inbox
        case projects
        case settings
    }
    
    var body: some View {
        TabView(selection: $selection){
            Text("This will be inbox")
                .font(.title)
                .tabItem {
                    VStack {
                        Image(systemName: "tray.fill")
                        Text("Inbox")
                    }
            }
            .tag(Tabs.inbox)
            Text("This will be projects")
                .font(.title)
                .tabItem {
                    VStack {
                        Image(systemName: "folder.fill")
                        Text("Projects")
                    }
            }
            .tag(Tabs.projects)
            Text("This will be settings")
                .font(.title)
                .tabItem {
                    VStack {
                        Image(systemName: "gear")
                        Text("Settings")
                    }
            }
            .tag(Tabs.settings)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
